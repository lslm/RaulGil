package raulgil;

public class Apresentacao {
    private Calouro calouro;
    private String titulo;
    private String descricao;
    
    public Apresentacao(Calouro calouro, String titulo, String descricao) {
        this.calouro = calouro;
        this.titulo = titulo;
        this.descricao = descricao;
    }

    public String getTítulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public Calouro getCalouro() {
        return calouro;
    }
}
